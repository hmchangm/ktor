# TODO API Server
## Features
- Language: Kotlin
- Framework: Ktor
- Database: Mongo

## Descritption
This is a todo api server, build with kotlin and ktor framework.
In demo page: http://0.0.0.0:8088/index.html, you can register and login your account.
Create new task, update exsit task, query task list with filter and sorting rules.
Auth part is using JWT OAtuh.

## Install
### Pre Condition
- With Mongo server at 127.0.0.1:27017

## Run
```sh
./gradlew run
```
- Demo Page: http://0.0.0.0:8088/index.html

## Test
```sh
./gradlew test
```

## OPEN API Doc
- src/main/resources/OpenAPI.json