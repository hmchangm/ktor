package com.wisely.modules

import com.wisely.routes.taskRouting
import com.wisely.routes.userRouting
import io.ktor.server.routing.*
import io.ktor.server.http.content.*
import io.ktor.server.application.*

fun Application.configureRouting() {
    routing {
        route("/api/v1") {
            taskRouting()
            userRouting()
        }

        // Static plugin. Try to access `/static/index.html`
        static("/") {
            resources("static")
        }
    }
}
