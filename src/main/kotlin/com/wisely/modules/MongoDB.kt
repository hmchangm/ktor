package com.wisely.modules

import com.wisely.models.Mongo
import io.ktor.server.application.*

fun Application.configMongoDB() {
    val url = environment.config.property("mongo.url").getString()
    val db = environment.config.property("mongo.db").getString()
    val testing = environment.config.property("mongo.testing").getString().toBooleanStrictOrNull() ?: false
    Mongo.setUp(url, db, testing)
}