package com.wisely.modules

import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import com.wisely.routes.Const.Companion.USER_ID
import com.wisely.routes.error.TSMCError.Companion.INVALID_TOKEN
import com.wisely.routes.response.ErrorResponse
import com.wisely.utils.CID
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.auth.jwt.*
import io.ktor.server.response.*

fun Application.configureSecurity() {

    authentication {
        jwt("jwt-auth") {
            val secret = environment.config.property("jwt.secret").getString()
            val audience = environment.config.property("jwt.audience").getString()
            val issuer = environment.config.property("jwt.issuer").getString()
            realm = environment.config.property("jwt.realm").getString()
            verifier(
                JWT.require(Algorithm.HMAC256(secret))
                    .withAudience(audience)
                    .withIssuer(issuer)
                    .build()
            )
            validate { credential ->
                if (credential.payload.getClaim(USER_ID).asString() != "") {
                    JWTPrincipal(credential.payload)
                } else {
                    null
                }
            }
            challenge { _, _ ->
                val cid = CID.gen()
                call.respond(
                    HttpStatusCode.Unauthorized,
                    ErrorResponse(cid, INVALID_TOKEN, "unauthorized")
                )
            }
        }
    }
}
