package com.wisely.models

import com.mongodb.client.MongoCollection
import org.bson.Document

interface ICollection {
    fun getCollectionName(): String
    fun createIndex(collection: MongoCollection<Document>)
}