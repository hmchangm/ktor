package com.wisely.models

import com.google.gson.annotations.SerializedName
import com.mongodb.client.MongoCollection
import com.mongodb.client.model.Filters.eq
import com.mongodb.client.model.IndexOptions
import com.mongodb.client.model.Indexes
import com.wisely.utils.Time
import org.bson.Document
import org.bson.conversions.Bson
import org.bson.types.ObjectId
import java.util.*

class UserCollection : ICollection {
    override fun getCollectionName(): String {
        return User.Collection
    }

    override fun createIndex(collection: MongoCollection<Document>) {
        collection.createIndex(Indexes.ascending(User.FDEMail), IndexOptions().unique(true))
    }
}

data class User(
    @SerializedName("_id")
    val id: String,
    val email: String,
    val password: String,
    val hash: String,
    val createdTime: String,
    val updatedTime: String,
) {
    companion object {
        const val Collection = "user"
        const val FDEMail = "email"
        const val FDPassword = "password"
        const val FDHash = "hash"

        fun getUserByEmail(email: String): User? {
            val filter = eq(FDEMail, email)
            return getOneUser(filter)
        }

        private fun getOneUser(filter: Bson): User? {
            val collection = Mongo.collections[Collection] ?: return null
            val userCursor = collection.find(filter).limit(1).iterator()
            var user: User? = null
            try {
                while (userCursor.hasNext()) {
                    userCursor.next().fromJson<User>(User::class.java)?.let {
                        user = it
                    }
                }
            } catch (e: Exception) {
                Mongo.logger.error("userCursor next error: $e")
            } finally {
                userCursor.close()
            }
            return user
        }

        fun insertUser(email: String, password: String, hash: String): String {
            val collection = Mongo.collections[Collection] ?: return ""
            val document = Document()
            val id = ObjectId()
            val now = Time.convertDateToString(Date())
            document.append(Mongo.ObjectID, id).append(FDEMail, email).append(FDPassword, password)
                .append(FDHash, hash).append(Mongo.FDCreatedTime, now).append(Mongo.FDUpdatedTime, now)
            var result = id.toHexString()
            try {
                collection.insertOne(document)
            } catch (e: Exception) {
                Mongo.logger.error("insertUser error: $e")
                result = ""
            }
            return result
        }
    }
}