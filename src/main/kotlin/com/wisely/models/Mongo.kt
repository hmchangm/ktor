package com.wisely.models


import com.google.gson.Gson
import com.mongodb.client.MongoClients
import com.mongodb.client.MongoCollection
import io.ktor.util.logging.*
import org.bson.Document
import org.bson.json.JsonMode
import org.bson.json.JsonWriterSettings
import org.bson.json.StrictJsonWriter
import org.bson.types.ObjectId
import org.slf4j.LoggerFactory
import java.lang.reflect.Type


object Mongo {
    const val ObjectID = "_id"
    const val ID = "id"
    const val FDCreatedTime = "createdTime"
    const val FDUpdatedTime = "updatedTime"
    const val SEP = "XX::XX"

    val collections: HashMap<String, MongoCollection<Document>> = HashMap()
    val jsonWriterSettings: JsonWriterSettings = JsonWriterSettings.builder().outputMode(JsonMode.RELAXED)
        .objectIdConverter { value: ObjectId, writer: StrictJsonWriter ->
            writer.writeString(
                value.toHexString()
            )
        }
        .build()
    lateinit var logger: Logger

    @Throws(Exception::class)
    fun setUp(url: String, db: String, test: Boolean = false) {
        this.logger = LoggerFactory.getLogger(Mongo::class.java)
        logger.debug("setUp, url: $url, db: $db, test: $test")
        val mongoClient = MongoClients.create("mongodb://$url")
        val database = mongoClient.getDatabase(db)
        val collectionList = listOf(
            UserCollection(),
            TaskCollection(),
        )
        for (c in collectionList) {
            val collectionName = c.getCollectionName()
            val collection = database.getCollection(collectionName)
            if (test) {
                collection.drop()
            }
            collections[collectionName] = collection
            c.createIndex(collection)
        }
    }
}

fun <T> Document.fromJson(typeOfT: Type): T? {
    val json = this.toJson(Mongo.jsonWriterSettings)
    val t: T? = try {
        Gson().fromJson(json, typeOfT)
    } catch (e: Exception) {
        null
    }
    return t
}
