package com.wisely.models

data class ListData<T>(
    val nextCursor: String,
    val data: List<T>
)