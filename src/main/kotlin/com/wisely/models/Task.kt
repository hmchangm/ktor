package com.wisely.models

import com.mongodb.client.MongoCollection
import com.mongodb.client.model.Filters.and
import com.mongodb.client.model.Filters.eq
import com.mongodb.client.model.FindOneAndUpdateOptions
import com.mongodb.client.model.IndexOptions
import com.mongodb.client.model.Indexes
import com.mongodb.client.model.ReturnDocument
import com.mongodb.client.model.Sorts.descending
import com.mongodb.client.model.Updates.combine
import com.mongodb.client.model.Updates.set
import com.wisely.models.Mongo.SEP
import com.wisely.utils.Time
import org.bson.*
import org.bson.types.ObjectId
import java.util.*

class TaskCollection : ICollection {
    override fun getCollectionName(): String {
        return Task.Collection
    }

    override fun createIndex(collection: MongoCollection<Document>) {
        collection.createIndex(
            Indexes.ascending(Mongo.ID),
            IndexOptions().unique(true)
        )
        collection.createIndex(
            Indexes.compoundIndex(
                Indexes.ascending(Task.FDOwner),
                Indexes.descending(Mongo.ID)
            )
        )

        collection.createIndex(
            Indexes.compoundIndex(
                Indexes.ascending(Task.FDOwner),
                Indexes.descending(Task.FDStatusValue),
                Indexes.descending(Mongo.ID)
            )
        )

        collection.createIndex(
            Indexes.compoundIndex(
                Indexes.ascending(Task.FDOwner),
                Indexes.descending(Task.FDPriorityValue),
                Indexes.descending(Mongo.ID)
            )
        )

        collection.createIndex(
            Indexes.compoundIndex(
                Indexes.ascending(Task.FDOwner),
                Indexes.descending(Task.FDStatusValue),
                Indexes.descending(Task.FDPriorityValue),
                Indexes.descending(Mongo.ID)
            )
        )

        collection.createIndex(
            Indexes.compoundIndex(
                Indexes.ascending(Task.FDOwner),
                Indexes.descending(Task.FDPriorityValue),
                Indexes.descending(Task.FDStatusValue),
                Indexes.descending(Mongo.ID)
            )
        )
    }
}

enum class SortType(val value: String) {
    Time("time"),
    StatusAndPriority("status"),
    PriorityAndStatus("priority");

    companion object {
        fun fromValue(value: String?): SortType? {
            return SortType.values().find { s -> s.value == value }
        }
    }
}

enum class Status(val value: Int) {
    Init(1000),
    Processing(700),
    Finished(400),
    Cancel(100);

    companion object {
        fun fromName(name: String?): Status? {
            return values().find { s -> s.name == name }
        }
    }
}

enum class Priority(val value: Int) {
    High(1000),
    Medium(700),
    Normal(400),
    Low(100);

    companion object {
        fun fromName(name: String?): Priority? {
            return values().find { p -> p.name == name }
        }
    }
}

data class Task(
    val id: String,
    val owner: String,
    val title: String,
    val status: Status,
    val statusValue: Int,
    val priority: Priority,
    val priorityValue: Int,
    val dueDate: String,
    val createdTime: String,
    val updatedTime: String
) {
    companion object {
        const val Collection = "task"
        private const val FDStatus = "status"
        const val FDStatusValue = "statusValue"
        private const val FDPriority = "priority"
        const val FDPriorityValue = "priorityValue"
        const val FDOwner = "owner"
        private const val FDTitle = "title"
        private const val FDDueDate = "dueDate"

        fun getTaskByID(id: String): Task? {
            val collection = Mongo.collections[Collection] ?: return null
            val filter = eq(Mongo.ID, id)
            val taskCursor = collection.find(filter).limit(1).iterator()
            var task: Task? = null
            try {
                while (taskCursor.hasNext()) {
                    taskCursor.next().fromJson<Task>(Task::class.java)?.let {
                        task = it
                    }
                }
            } catch (e: Exception) {
                Mongo.logger.error("taskCursor next error: $e")
            } finally {
                taskCursor.close()
            }
            return task
        }

        fun getTaskByOwner(
            owner: String,
            status: Status? = null,
            priority: Priority? = null,
            limit: Int = 20,
            sortType: SortType = SortType.Time,
            cursor: String = ""
        ): ListData<Task> {
            val collection = Mongo.collections[Collection] ?: return ListData("", emptyList())
            val filter = BsonDocument()
            filter[FDOwner] = BsonString(owner)
            status?.let {
                filter[FDStatus] = BsonString(it.name)
            }
            priority?.let {
                filter[FDPriority] = BsonString(it.name)
            }
            getLoadMoreCursor(filter, cursor, sortType)
            var sort = descending(Mongo.ID)
            if (sortType == SortType.StatusAndPriority) {
                sort = descending(FDStatusValue, FDPriorityValue, Mongo.ID)
            } else if (sortType == SortType.PriorityAndStatus) {
                sort = descending(FDPriorityValue, FDStatusValue, Mongo.ID)
            }
            val result = mutableListOf<Task>()
            val taskCursor = collection.find(filter).limit(limit + 1).sort(sort).iterator()
            try {
                while (taskCursor.hasNext()) {
                    taskCursor.next().fromJson<Task>(Task::class.java)?.let {
                        result.add(it)
                    }
                }
            } catch (e: Exception) {
                Mongo.logger.error("taskCursor next error: $e")
            } finally {
                taskCursor.close()
            }
            var nextCursor = ""
            if (result.size == limit + 1) {
                val last = result.removeLast()
                nextCursor = getNextCursor(sortType, last)
            }
            return ListData(nextCursor, result)
        }

        fun insertTask(owner: String, title: String, priority: Priority, dueDate: Date): Task? {
            val collection = Mongo.collections[Collection] ?: return null
            val document = Document()
            val id = ObjectId()
            val now = Time.convertDateToString(Date())
            val dueDateStr = Time.convertDateToSimpleString(dueDate)
            document.append(Mongo.ObjectID, id)
                .append(Mongo.ID, id.toHexString())
                .append(FDOwner, owner)
                .append(FDStatus, Status.Init)
                .append(FDStatusValue, Status.Init.value)
                .append(FDTitle, title)
                .append(FDPriority, priority)
                .append(FDPriorityValue, priority.value)
                .append(FDDueDate, dueDateStr)
                .append(Mongo.FDCreatedTime, now)
                .append(Mongo.FDUpdatedTime, now)
            var result: Task? = Task(
                id = id.toHexString(),
                owner = owner,
                status = Status.Init,
                statusValue = Status.Init.value,
                priority = priority,
                priorityValue = priority.value,
                dueDate = dueDateStr,
                title = title,
                createdTime = now,
                updatedTime = now,
            )
            try {
                collection.insertOne(document)
            } catch (e: Exception) {
                Mongo.logger.error("insertTask error: $e")
                result = null
            }
            return result
        }

        fun updateTask(
            id: String,
            oldStatus: Status,
            newStatus: Status?,
            priority: Priority?,
            title: String?,
            dueDate: Date?
        ): Task? {
            val collection = Mongo.collections[Collection] ?: return null
            var filter = eq(Mongo.ID, id)
            var update = set(Mongo.FDUpdatedTime, Time.convertDateToString(Date()))
            newStatus?.let {
                filter = and(filter, eq(FDStatus, oldStatus))
                update = combine(set(FDStatus, it), update)
                update = combine(set(FDStatusValue, it.value), update)
            }
            priority?.let {
                update = combine(set(FDPriority, it), update)
                update = combine(set(FDPriorityValue, it.value), update)
            }
            title?.let {
                update = combine(set(FDTitle, it), update)
            }
            dueDate?.let {
                update = combine(set(FDDueDate, Time.convertDateToSimpleString(it)), update)
            }
            val options = FindOneAndUpdateOptions().returnDocument(ReturnDocument.AFTER)
            return try {
                val document = collection.findOneAndUpdate(filter, update, options)
                document?.fromJson(Task::class.java)
            } catch (e: Exception) {
                Mongo.logger.error("updateTaskStatus error: $e")
                null
            }
        }

        private fun getLoadMoreCursor(filter: BsonDocument, cursor: String, sortType: SortType) {
            if (cursor.isEmpty()) {
                return
            }
            when (sortType) {
                SortType.StatusAndPriority -> {
                    val tempCursor = cursor.split(SEP)
                    if (tempCursor.size != 3) {
                        return
                    }
                    val status = Status.fromName(tempCursor[0])
                    val priority = Priority.fromName(tempCursor[1])
                    if (status == null || priority == null) {
                        return
                    }
                    val id = tempCursor[2]
                    val filter1 = BsonDocument()
                    filter1[FDStatusValue] = BsonInt32(status.value)
                    filter1[FDPriorityValue] = BsonInt32(priority.value)
                    val lteDoc1 = BsonDocument()
                    lteDoc1["\$lte"] = BsonString(id)
                    filter1[Mongo.ID] = lteDoc1

                    val filter2 = BsonDocument()
                    filter2[FDStatusValue] = BsonInt32(status.value)
                    val lteDoc2 = BsonDocument()
                    lteDoc2["\$lt"] = BsonInt32(priority.value)
                    filter2[FDPriorityValue] = lteDoc2

                    val filter3 = BsonDocument()
                    val lteDoc3 = BsonDocument()
                    lteDoc3["\$lt"] = BsonInt32(status.value)
                    filter3[FDStatusValue] = lteDoc3

                    val orDoc = BsonArray()
                    orDoc.add(filter1)
                    orDoc.add(filter2)
                    orDoc.add(filter3)
                    filter["\$or"] = orDoc
                }
                SortType.PriorityAndStatus -> {
                    val tempCursor = cursor.split(SEP)
                    if (tempCursor.size != 3) {
                        return
                    }
                    val priority = Priority.fromName(tempCursor[0])
                    val status = Status.fromName(tempCursor[1])
                    if (status == null || priority == null) {
                        return
                    }
                    val id = tempCursor[2]
                    val filter1 = BsonDocument()
                    filter1[FDPriorityValue] = BsonInt32(priority.value)
                    filter1[FDStatusValue] = BsonInt32(status.value)
                    val lteDoc1 = BsonDocument()
                    lteDoc1["\$lte"] = BsonString(id)
                    filter1[Mongo.ID] = lteDoc1

                    val filter2 = BsonDocument()
                    filter2[FDPriorityValue] = BsonInt32(priority.value)
                    val lteDoc2 = BsonDocument()
                    lteDoc2["\$lt"] = BsonInt32(status.value)
                    filter2[FDStatusValue] = lteDoc2

                    val filter3 = BsonDocument()
                    val lteDoc3 = BsonDocument()
                    lteDoc3["\$lt"] = BsonInt32(priority.value)
                    filter3[FDPriorityValue] = lteDoc3

                    val orDoc = BsonArray()
                    orDoc.add(filter1)
                    orDoc.add(filter2)
                    orDoc.add(filter3)
                    filter["\$or"] = orDoc
                }
                else -> {
                    val lteDoc = BsonDocument()
                    lteDoc["\$lte"] = BsonString(cursor)
                    filter[Mongo.ID] = lteDoc
                }
            }
        }

        private fun getNextCursor(sortType: SortType, task: Task): String {
            val nextCursor: String = when (sortType) {
                SortType.StatusAndPriority -> {
                    "${task.status}${SEP}${task.priority}${SEP}${task.id}"
                }
                SortType.PriorityAndStatus -> {
                    "${task.priority}${SEP}${task.status}${SEP}${task.id}"
                }
                else -> {
                    task.id
                }
            }
            return nextCursor
        }
    }
}
