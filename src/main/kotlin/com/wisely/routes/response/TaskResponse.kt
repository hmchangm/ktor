package com.wisely.routes.response

import com.wisely.models.Status
import com.wisely.models.Task

data class TaskResponse(
    val id: String,
    val owner: String,
    val title: String,
    val status: Status,
    val createdTime: String,
    val updatedTime: String
)

data class TaskListResponse(
    val list: List<Task>,
    val nextCursor: String = "",
)