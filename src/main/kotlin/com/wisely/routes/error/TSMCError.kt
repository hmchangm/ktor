package com.wisely.routes.error

class TSMCError {
    companion object {
        const val INVALID_TOKEN = 1000000
        const val INVALID_INPUT = 1000001

        const val REGISTER_USER_INSERT_FAILED = 1100001
        const val REGISTER_ENCRYPT_FAILED = 1100002

        const val LOGIN_USER_NOT_FOUND = 1200001
        const val LOGIN_INVALID_PASSWORD = 1200002

        const val TASK_NOT_FOUND = 1300001
        const val TASK_INSERT_FAILED = 1300002
        const val TASK_STATUS_MISMATCH = 1300003
        const val TASK_UPDATE_FAILED = 1300004
    }
}