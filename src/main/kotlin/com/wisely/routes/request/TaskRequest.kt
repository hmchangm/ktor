package com.wisely.routes.request

data class CreateTaskRequest(
    val title: String = "",
    val priority: String = "Low",
    val dueDate: String,
)

data class UpdateTaskRequest(
    val status: String? = null,
    val title: String? = null,
    val priority: String? = null,
    val dueDate: String? = null,
)
