package com.wisely.routes.utils

import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import com.wisely.routes.Const
import com.wisely.utils.Time
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.auth.jwt.*
import java.util.*

data class JWTResult(
    val userId: String,
    val isValid: Boolean,
)

data class JWTToken(
    val token: String,
    val expired: Long,
)

class JWT {
    companion object {
        fun checkJWTToken(call: ApplicationCall): JWTResult {
            val principal = call.principal<JWTPrincipal>() ?: return JWTResult("", false)
            val userId = principal.payload.getClaim(Const.USER_ID).asString()
            val isValid = (principal.expiresAt?.time?.minus(System.currentTimeMillis()) ?: 0) > 0
            return JWTResult(userId, isValid)
        }

        fun createToken(application: Application, userId: String): JWTToken {
            val secret = application.environment.config.property("jwt.secret").getString()
            val issuer = application.environment.config.property("jwt.issuer").getString()
            val audience = application.environment.config.property("jwt.audience").getString()
            val expired = System.currentTimeMillis() + (8L * Time.Minute)
            val actualExpired = System.currentTimeMillis() + (10L * Time.Minute)
            val token = JWT.create().withAudience(audience).withIssuer(issuer).withClaim(Const.USER_ID, userId)
                .withExpiresAt(Date(actualExpired)).sign(Algorithm.HMAC256(secret))
            return JWTToken(token, expired)
        }
    }
}