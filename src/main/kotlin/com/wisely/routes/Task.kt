package com.wisely.routes

import com.wisely.models.Priority
import com.wisely.models.SortType
import com.wisely.models.Status
import com.wisely.models.Task
import com.wisely.routes.Const.Companion.CURSOR
import com.wisely.routes.Const.Companion.ID
import com.wisely.routes.Const.Companion.LIMIT
import com.wisely.routes.Const.Companion.PRIORITY
import com.wisely.routes.Const.Companion.SORT
import com.wisely.routes.Const.Companion.STATUS
import com.wisely.routes.error.TSMCError.Companion.INVALID_INPUT
import com.wisely.routes.error.TSMCError.Companion.INVALID_TOKEN
import com.wisely.routes.error.TSMCError.Companion.TASK_INSERT_FAILED
import com.wisely.routes.error.TSMCError.Companion.TASK_NOT_FOUND
import com.wisely.routes.error.TSMCError.Companion.TASK_STATUS_MISMATCH
import com.wisely.routes.error.TSMCError.Companion.TASK_UPDATE_FAILED
import com.wisely.routes.request.CreateTaskRequest
import com.wisely.routes.request.UpdateTaskRequest
import com.wisely.routes.response.ErrorResponse
import com.wisely.routes.response.SuccessResponse
import com.wisely.routes.response.TaskListResponse
import com.wisely.routes.utils.JWT.Companion.checkJWTToken
import com.wisely.utils.CID
import com.wisely.utils.Time
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

fun Route.taskRouting() {
    authenticate("jwt-auth") {
        route("/tasks") {
            taskByIdRoute()
            createTaskRoute()
            getTaskListRoute()
            updateTaskByIdRoute()
        }
    }
}

fun Route.taskByIdRoute() {
    get("/{id}") {
        val cid = CID.gen()
        val jwtResult = checkJWTToken(call)
        if (!jwtResult.isValid) {
            call.respond(HttpStatusCode.Unauthorized, ErrorResponse(cid, INVALID_TOKEN))
            return@get
        }
        val taskId = call.parameters[ID]
        if (taskId.isNullOrEmpty()) {
            call.respond(HttpStatusCode.BadRequest, ErrorResponse(cid, INVALID_INPUT, "invalid input"))
            return@get
        }
        val task = Task.getTaskByID(taskId)
        if (task == null) {
            call.respond(HttpStatusCode.NotFound, ErrorResponse(cid, TASK_NOT_FOUND, "task not found"))
            return@get
        }
        call.respond(HttpStatusCode.OK, SuccessResponse(cid, task))
    }
}

fun Route.createTaskRoute() {
    post("") {
        val cid = CID.gen()
        val jwtResult = checkJWTToken(call)
        if (!jwtResult.isValid) {
            call.respond(HttpStatusCode.Unauthorized, ErrorResponse(cid, INVALID_TOKEN))
            return@post
        }
        val createTaskRequest = call.receive<CreateTaskRequest>()
        if (createTaskRequest.title.isBlank()) {
            call.respond(HttpStatusCode.BadRequest, ErrorResponse(cid, INVALID_INPUT, "invalid input"))
            return@post
        }
        val date = Time.convertFromDateString(createTaskRequest.dueDate)
        if (date == null) {
            call.respond(HttpStatusCode.BadRequest, ErrorResponse(cid, INVALID_INPUT, "invalid input"))
            return@post
        }
        val priority = Priority.fromName(createTaskRequest.priority) ?: Priority.Low
        val insertTask = Task.insertTask(jwtResult.userId, createTaskRequest.title, priority, date)
        if (insertTask == null) {
            call.respond(HttpStatusCode.BadRequest, ErrorResponse(cid, TASK_INSERT_FAILED, "insert task error"))
            return@post
        }
        call.respond(HttpStatusCode.OK, SuccessResponse(cid, insertTask))
    }
}

fun Route.getTaskListRoute() {
    get("") {
        val cid = CID.gen()
        val jwtResult = checkJWTToken(call)
        if (!jwtResult.isValid) {
            call.respond(HttpStatusCode.Unauthorized, ErrorResponse(cid, INVALID_TOKEN))
            return@get
        }
        val status: Status? = call.request.queryParameters[STATUS]?.let {
            Status.fromName(it)
        } ?: run {
            null
        }
        val priority: Priority? = call.request.queryParameters[PRIORITY]?.let {
            Priority.fromName(it)
        } ?: run {
            null
        }
        val sort: SortType = call.request.queryParameters[SORT]?.let {
            SortType.fromValue(it)
        } ?: run {
            SortType.Time
        }
        val cursor: String = call.request.queryParameters[CURSOR] ?: ""
        val limit = call.request.queryParameters[LIMIT]?.toIntOrNull() ?: 20
        val taskList = Task.getTaskByOwner(jwtResult.userId, status, priority, limit, sort, cursor)
        val taskListResponse = TaskListResponse(
            list = taskList.data,
            nextCursor = taskList.nextCursor,
        )
        call.respond(HttpStatusCode.OK, SuccessResponse(cid, taskListResponse))
    }
}

fun Route.updateTaskByIdRoute() {
    patch("/{id}") {
        val cid = CID.gen()
        val jwtResult = checkJWTToken(call)
        if (!jwtResult.isValid) {
            call.respond(HttpStatusCode.Unauthorized, ErrorResponse(cid, INVALID_TOKEN))
            return@patch
        }
        val taskId = call.parameters[ID]
        if (taskId.isNullOrEmpty()) {
            call.respond(HttpStatusCode.BadRequest, ErrorResponse(cid, INVALID_INPUT, "invalid input"))
            return@patch
        }
        val oldTask = Task.getTaskByID(taskId)
        if (oldTask == null) {
            call.respond(HttpStatusCode.NotFound, ErrorResponse(cid, TASK_NOT_FOUND, "task not found"))
            return@patch
        }
        val updateTaskRequest = call.receive<UpdateTaskRequest>()
        val newStatus = Status.fromName(updateTaskRequest.status)
        if (!validateStatus(oldTask.status, newStatus)) {
            call.respond(HttpStatusCode.Forbidden, ErrorResponse(cid, TASK_STATUS_MISMATCH, "task status mismatch"))
            return@patch
        }
        val newPriority = Priority.fromName(updateTaskRequest.priority)
        val newDate = Time.convertFromDateString(updateTaskRequest.dueDate)
        val newTask = Task.updateTask(taskId, oldTask.status, newStatus, newPriority, updateTaskRequest.title, newDate)
        if (newTask == null) {
            call.respond(HttpStatusCode.BadRequest, ErrorResponse(cid, TASK_UPDATE_FAILED, "update task error"))
            return@patch
        }
        call.respond(HttpStatusCode.OK, SuccessResponse(cid, newTask))
    }
}

private fun validateStatus(oldStatus: Status, newStatus: Status?): Boolean {
    val isValid: Boolean
    when (newStatus) {
        Status.Cancel -> {
            isValid = (oldStatus == Status.Init || oldStatus == Status.Processing)
        }
        Status.Processing -> {
            isValid = (oldStatus == Status.Init)
        }
        Status.Finished -> {
            isValid = (oldStatus == Status.Processing)
        }
        Status.Init -> {
            isValid = (oldStatus == Status.Cancel)
        }
        else -> {
            isValid = true
        }
    }
    return isValid
}