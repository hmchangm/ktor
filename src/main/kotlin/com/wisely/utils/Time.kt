package com.wisely.utils

import java.text.SimpleDateFormat
import java.util.*

class Time {
    companion object {
        const val Minute = 60L * 1000L

        private val dateFormat = SimpleDateFormat("yyyy-MM-dd")
        private val fullTimeFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ")

        init {
            dateFormat.timeZone = TimeZone.getTimeZone("UTC")
            fullTimeFormat.timeZone = TimeZone.getTimeZone("UTC")
        }

        fun convertFromDateString(dateString: String?): Date? {
            return try {
                dateFormat.parse(dateString)
            } catch (e: Exception) {
                null
            }
        }

        fun convertDateToSimpleString(date: Date): String {
            return dateFormat.format(date)
        }

        fun convertDateToString(date: Date): String {
            return fullTimeFormat.format(date)
        }
    }
}