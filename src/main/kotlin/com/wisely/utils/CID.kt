package com.wisely.utils

import java.util.*

class CID {
    companion object {
        fun gen(): String {
            return "${System.currentTimeMillis()}T${UUID.randomUUID().toString().replace("-", "")}"
        }
    }
}