function registerAPI(email, password) {
    if (!email || !password) {
        console.error("register empty email or password");
        return null;
    }
    let data = {email, password};
    return new Promise((resolve) => {
        $.ajax({
            url: `/api/v1/register`,
            method: "POST",
            data: JSON.stringify(data),
            dataType: "json",
            contentType: "application/json",
            success: function (data) {
                console.log("register success: ", data);
                resolve(data);
            },
            error: function (err) {
                console.error("register error: ", err);
                resolve(err["responseJSON"]);
            }
        });
    });
}


function loginAPI(email, password) {
    if (!email || !password) {
        console.error("login empty email or password");
        return null;
    }
    let data = {email, password};
    return new Promise((resolve) => {
        $.ajax({
            url: `/api/v1/login`,
            method: "POST",
            data: JSON.stringify(data),
            dataType: "json",
            contentType: "application/json",
            success: function (data) {
                console.log("login success: ", data);
                resolve(data);
            },
            error: function (err) {
                console.error("login error: ", err);
                resolve(err["responseJSON"]);
            }
        });
    });
}

function tokenAPI() {
    let token = sessionStorage.getItem("token");
    return new Promise((resolve) => {
        $.ajax({
            url: `/api/v1/token`,
            method: "POST",
            headers: {
                "Authorization": `Bearer ${token}`,
            },
            success: function (data) {
                console.log("exchange token success: ", data);
                resolve(data);
            },
            error: function (err) {
                console.error("exchange token error: ", err);
                resolve(err["responseJSON"]);
            }
        });
    });
}