function getTaskListAPI(status, priority, sort = "time", nextCursor = "") {
    let token = sessionStorage.getItem("token");
    return new Promise((resolve) => {
        $.ajax({
            url: `/api/v1/tasks?status=${status}&priority=${priority}&sort=${sort}&cursor=${nextCursor}`,
            method: "GET",
            headers: {
                "Authorization": `Bearer ${token}`,
            },
            success: function (data) {
                console.log("get task list success: ", data);
                resolve(data);
            },
            error: function (err) {
                console.error("get task list error: ", err);
                resolve(err["responseJSON"]);
            }
        });
    });
}

function createTaskAPI(title, priority, dueDate) {
    let token = sessionStorage.getItem("token");
    if (!title || !priority || !dueDate) {
        console.error("create task empty title, priority or dueDate");
        return null;
    }
    let data = {title, priority, dueDate};
    return new Promise((resolve) => {
        $.ajax({
            url: `/api/v1/tasks`,
            method: "POST",
            headers: {
                "Authorization": `Bearer ${token}`,
            },
            data: JSON.stringify(data),
            dataType: "json",
            contentType: "application/json",
            success: function (data) {
                console.log("create task success: ", data);
                resolve(data);
            },
            error: function (err) {
                console.error("create task error: ", err);
                resolve(err["responseJSON"]);
            }
        });
    });
}

function modifyTaskAPI(id, status, title, priority, dueDate) {
    if (!id) {
        return null;
    }
    let token = sessionStorage.getItem("token");
    let data = {status, title, priority, dueDate};
    return new Promise((resolve) => {
        $.ajax({
            url: `/api/v1/tasks/${id}`,
            method: "PATCH",
            headers: {
                "Authorization": `Bearer ${token}`,
            },
            data: JSON.stringify(data),
            dataType: "json",
            contentType: "application/json",
            success: function (data) {
                console.log("update task success: ", data);
                resolve(data);
            },
            error: function (err) {
                console.error("update task error: ", err);
                resolve(err["responseJSON"]);
            }
        });
    });
}