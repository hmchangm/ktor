package com.wisely.models

import com.wisely.models.Mongo.SEP
import com.wisely.utils.Time
import org.bson.types.ObjectId
import org.junit.BeforeClass
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.runners.MethodSorters
import java.util.*
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
class TaskTest {

    @Test
    fun test00InsertTask() {
        val dateString = "2022-04-04+0000"
        val date = Time.convertFromDateString(dateString)
        val insertTask = Task.insertTask(owner0, title0, Priority.Low, date!!)
        assertNotNull(insertTask)
        assertEquals(title0, insertTask.title)
        assertEquals(owner0, insertTask.owner)
        assertEquals(Priority.Low, insertTask.priority)
        assertEquals("2022-04-04", insertTask.dueDate)
    }

    @Test
    fun test01GetTaskByOwner() {
        var taskList = Task.getTaskByOwner(owner0, Status.Init)
        assertEquals("", taskList.nextCursor)
        assertEquals(1, taskList.data.size)
        val task = taskList.data[0]
        assertEquals(owner0, task.owner)
        assertEquals(title0, task.title)
        assertEquals(Status.Init, task.status)

        taskList = Task.getTaskByOwner(owner0, Status.Processing)
        assertEquals("", taskList.nextCursor)
        assertEquals(0, taskList.data.size)

        taskList = Task.getTaskByOwner(ownerNotExist, Status.Init)
        assertEquals("", taskList.nextCursor)
        assertEquals(0, taskList.data.size)

        for (i in 1..200) {
            val priority = when (i % 4) {
                0 -> Priority.Low
                1 -> Priority.Normal
                2 -> Priority.Medium
                else -> Priority.High
            }
            val insertTask = Task.insertTask(owner0, "title0-$i", priority, Date())
            assertNotNull(insertTask)
        }
        taskList = Task.getTaskByOwner(owner0, Status.Init, limit = 10)
        assertTrue(taskList.nextCursor.isNotEmpty())
        assertEquals(10, taskList.data.size)

        taskList = Task.getTaskByOwner(owner0, Status.Init, null, limit = 200, SortType.Time, taskList.nextCursor)
        assertTrue(taskList.nextCursor.isEmpty())
        assertEquals(191, taskList.data.size)
        assertEquals(taskList.data[190].createdTime, task.createdTime)

        taskList = Task.getTaskByOwner(owner0, Status.Init, Priority.Low, limit = 100)
        assertTrue(taskList.nextCursor.isEmpty())
        assertEquals(51, taskList.data.size)

        taskList = Task.getTaskByOwner(owner0, Status.Init, Priority.High, limit = 100)
        assertTrue(taskList.nextCursor.isEmpty())
        assertEquals(50, taskList.data.size)
    }

    @Test
    fun test02GetTaskByOwner() {
        // each priority has 80 items
        for (i in 0 until 320) {
            val priority = when (i % 32) {
                in 0..7 -> Priority.Low
                in 8..15 -> Priority.Normal
                in 16..23 -> Priority.Medium
                else -> Priority.High
            }
            val insertTask = Task.insertTask(owner1, "$priority-title1-$i", priority, Date())
            assertNotNull(insertTask)
        }
        var taskList = Task.getTaskByOwner(owner1, Status.Init, limit = 320)
        assertTrue(taskList.nextCursor.isEmpty())
        assertEquals(320, taskList.data.size)

        // each priority status has 20 items
        taskList.data.forEachIndexed { i, task ->
            val status = when (i % 8) {
                in 0..1 -> Status.Init
                in 2..3 -> Status.Processing
                in 4..5 -> Status.Finished
                else -> Status.Cancel
            }
            val updatedTask = Task.updateTask(task.id, task.status, status, null, null, null)
            assertNotNull(updatedTask)
            assertEquals(status, updatedTask.status)
            assertEquals(task.priority, updatedTask.priority)
        }

        taskList = Task.getTaskByOwner(owner1, Status.Init, Priority.Low, sortType = SortType.Time, limit = 10)
        var nextCursor = taskList.nextCursor
        assertTrue(nextCursor.isNotEmpty())
        assertTrue(ObjectId.isValid(nextCursor))
        assertEquals(10, taskList.data.size)

        taskList =
            Task.getTaskByOwner(owner1, Status.Init, Priority.Low, sortType = SortType.PriorityAndStatus, limit = 10)
        nextCursor = taskList.nextCursor
        assertTrue(nextCursor.isNotEmpty())
        var cs = nextCursor.split(SEP)
        assertEquals(3, cs.size)
        assertEquals(cs[0], Priority.Low.name)
        assertEquals(cs[1], Status.Init.name)
        assertEquals(10, taskList.data.size)

        taskList = Task.getTaskByOwner(owner1, Status.Init, null, sortType = SortType.PriorityAndStatus, limit = 10)
        nextCursor = taskList.nextCursor
        assertTrue(nextCursor.isNotEmpty())
        cs = nextCursor.split(SEP)
        assertEquals(3, cs.size)
        assertEquals(cs[0], Priority.High.name)
        assertEquals(cs[1], Status.Init.name)
        assertEquals(10, taskList.data.size)

        taskList = Task.getTaskByOwner(owner1, Status.Init, null, sortType = SortType.PriorityAndStatus, limit = 25)
        nextCursor = taskList.nextCursor
        println(nextCursor)
        assertTrue(nextCursor.isNotEmpty())
        cs = nextCursor.split(SEP)
        assertEquals(3, cs.size)
        assertEquals(cs[0], Priority.Medium.name)
        assertEquals(cs[1], Status.Init.name)
        assertEquals(25, taskList.data.size)
        for (i in 0 until 25) {
            val task = taskList.data[i]
            if (i < 20) {
                assertEquals(Priority.High, task.priority)
            } else {
                assertEquals(Priority.Medium, task.priority)
            }
        }

        taskList = Task.getTaskByOwner(
            owner1,
            Status.Init,
            null,
            sortType = SortType.PriorityAndStatus,
            limit = 25,
            cursor = nextCursor
        )
        nextCursor = taskList.nextCursor
        println(nextCursor)
        assertTrue(nextCursor.isNotEmpty())
        cs = nextCursor.split(SEP)
        assertEquals(3, cs.size)
        assertEquals(cs[0], Priority.Normal.name)
        assertEquals(cs[1], Status.Init.name)
        assertEquals(25, taskList.data.size)
        for (i in 0 until 25) {
            val task = taskList.data[i]
            if (i < 15) {
                assertEquals(Priority.Medium, task.priority)
            } else {
                assertEquals(Priority.Normal, task.priority)
            }
        }

        taskList = Task.getTaskByOwner(
            owner1,
            Status.Init,
            null,
            sortType = SortType.PriorityAndStatus,
            limit = 25,
            cursor = nextCursor
        )
        nextCursor = taskList.nextCursor
        println(nextCursor)
        assertTrue(nextCursor.isNotEmpty())
        cs = nextCursor.split(SEP)
        assertEquals(3, cs.size)
        assertEquals(cs[0], Priority.Low.name)
        assertEquals(cs[1], Status.Init.name)
        assertEquals(25, taskList.data.size)
        for (i in 0 until 25) {
            val task = taskList.data[i]
            if (i < 10) {
                assertEquals(Priority.Normal, task.priority)
            } else {
                assertEquals(Priority.Low, task.priority)
            }
        }

        taskList = Task.getTaskByOwner(
            owner1,
            Status.Init,
            null,
            sortType = SortType.PriorityAndStatus,
            limit = 25,
            cursor = nextCursor
        )
        nextCursor = taskList.nextCursor
        println(nextCursor)
        assertTrue(nextCursor.isEmpty())
        assertEquals(5, taskList.data.size)
        for (i in 0 until 5) {
            val task = taskList.data[i]
            assertEquals(Priority.Low, task.priority)
        }
    }

    @Test
    fun test03GetTaskByOwner2() {
        var taskList =
            Task.getTaskByOwner(owner1, null, Priority.High, sortType = SortType.PriorityAndStatus, limit = 25)
        var nextCursor = taskList.nextCursor
        println(nextCursor)
        assertTrue(nextCursor.isNotEmpty())
        var cs = nextCursor.split(SEP)
        assertEquals(3, cs.size)
        assertEquals(cs[0], Priority.High.name)
        assertEquals(cs[1], Status.Processing.name)
        assertEquals(25, taskList.data.size)
        for (i in 0 until 25) {
            val task = taskList.data[i]
            if (i < 20) {
                assertEquals(Status.Init, task.status)
            } else {
                assertEquals(Status.Processing, task.status)
            }
        }

        taskList = Task.getTaskByOwner(
            owner1,
            null,
            Priority.High,
            sortType = SortType.PriorityAndStatus,
            limit = 25,
            cursor = nextCursor
        )
        nextCursor = taskList.nextCursor
        println(nextCursor)
        assertTrue(nextCursor.isNotEmpty())
        cs = nextCursor.split(SEP)
        assertEquals(3, cs.size)
        assertEquals(cs[0], Priority.High.name)
        assertEquals(cs[1], Status.Finished.name)
        assertEquals(25, taskList.data.size)
        for (i in 0 until 25) {
            val task = taskList.data[i]
            if (i < 15) {
                assertEquals(Status.Processing, task.status)
            } else {
                assertEquals(Status.Finished, task.status)
            }
        }

        taskList = Task.getTaskByOwner(
            owner1,
            null,
            Priority.High,
            sortType = SortType.PriorityAndStatus,
            limit = 25,
            cursor = nextCursor
        )
        nextCursor = taskList.nextCursor
        println(nextCursor)
        assertTrue(nextCursor.isNotEmpty())
        cs = nextCursor.split(SEP)
        assertEquals(3, cs.size)
        assertEquals(cs[0], Priority.High.name)
        assertEquals(cs[1], Status.Cancel.name)
        assertEquals(25, taskList.data.size)
        for (i in 0 until 25) {
            val task = taskList.data[i]
            if (i < 10) {
                assertEquals(Status.Finished, task.status)
            } else {
                assertEquals(Status.Cancel, task.status)
            }
        }

        taskList = Task.getTaskByOwner(
            owner1,
            null,
            Priority.High,
            sortType = SortType.PriorityAndStatus,
            limit = 25,
            cursor = nextCursor
        )
        nextCursor = taskList.nextCursor
        println(nextCursor)
        assertTrue(nextCursor.isEmpty())
        assertEquals(5, taskList.data.size)
        for (i in 0 until 5) {
            val task = taskList.data[i]
            assertEquals(Status.Cancel, task.status)
        }
    }

    @Test
    fun test04GetTaskByOwner3() {
        var taskList =
            Task.getTaskByOwner(owner1, null, Priority.High, sortType = SortType.StatusAndPriority, limit = 25)
        var nextCursor = taskList.nextCursor
        println(nextCursor)
        assertTrue(nextCursor.isNotEmpty())
        var cs = nextCursor.split(SEP)
        assertEquals(3, cs.size)
        assertEquals(cs[0], Status.Processing.name)
        assertEquals(cs[1], Priority.High.name)
        assertEquals(25, taskList.data.size)
        for (i in 0 until 25) {
            val task = taskList.data[i]
            if (i < 20) {
                assertEquals(Status.Init, task.status)
            } else {
                assertEquals(Status.Processing, task.status)
            }
        }

        taskList = Task.getTaskByOwner(
            owner1,
            null,
            Priority.High,
            sortType = SortType.StatusAndPriority,
            limit = 25,
            cursor = nextCursor
        )
        nextCursor = taskList.nextCursor
        println(nextCursor)
        assertTrue(nextCursor.isNotEmpty())
        cs = nextCursor.split(SEP)
        assertEquals(3, cs.size)
        assertEquals(cs[0], Status.Finished.name)
        assertEquals(cs[1], Priority.High.name)
        assertEquals(25, taskList.data.size)
        for (i in 0 until 25) {
            val task = taskList.data[i]
            if (i < 15) {
                assertEquals(Status.Processing, task.status)
            } else {
                assertEquals(Status.Finished, task.status)
            }
        }

        taskList = Task.getTaskByOwner(
            owner1,
            null,
            Priority.High,
            sortType = SortType.StatusAndPriority,
            limit = 25,
            cursor = nextCursor
        )
        nextCursor = taskList.nextCursor
        println(nextCursor)
        assertTrue(nextCursor.isNotEmpty())
        cs = nextCursor.split(SEP)
        assertEquals(3, cs.size)
        assertEquals(cs[0], Status.Cancel.name)
        assertEquals(cs[1], Priority.High.name)
        assertEquals(25, taskList.data.size)
        for (i in 0 until 25) {
            val task = taskList.data[i]
            if (i < 10) {
                assertEquals(Status.Finished, task.status)
            } else {
                assertEquals(Status.Cancel, task.status)
            }
        }

        taskList = Task.getTaskByOwner(
            owner1,
            null,
            Priority.High,
            sortType = SortType.StatusAndPriority,
            limit = 25,
            cursor = nextCursor
        )
        nextCursor = taskList.nextCursor
        println(nextCursor)
        assertTrue(nextCursor.isEmpty())
        assertEquals(5, taskList.data.size)
        for (i in 0 until 5) {
            val task = taskList.data[i]
            assertEquals(Status.Cancel, task.status)
        }
    }

    @Test
    fun test05GetTaskByOwner4() {
        var taskList =
            Task.getTaskByOwner(owner1, Status.Processing, null, sortType = SortType.StatusAndPriority, limit = 25)
        var nextCursor = taskList.nextCursor
        println(nextCursor)
        assertTrue(nextCursor.isNotEmpty())
        var cs = nextCursor.split(SEP)
        assertEquals(3, cs.size)
        assertEquals(cs[0], Status.Processing.name)
        assertEquals(cs[1], Priority.Medium.name)
        assertEquals(25, taskList.data.size)
        for (i in 0 until 25) {
            val task = taskList.data[i]
            if (i < 20) {
                assertEquals(Priority.High, task.priority)
            } else {
                assertEquals(Priority.Medium, task.priority)
            }
        }

        taskList = Task.getTaskByOwner(
            owner1,
            Status.Processing,
            null,
            sortType = SortType.StatusAndPriority,
            limit = 25,
            cursor = nextCursor
        )
        nextCursor = taskList.nextCursor
        println(nextCursor)
        assertTrue(nextCursor.isNotEmpty())
        cs = nextCursor.split(SEP)
        assertEquals(3, cs.size)
        assertEquals(cs[0], Status.Processing.name)
        assertEquals(cs[1], Priority.Normal.name)
        assertEquals(25, taskList.data.size)
        for (i in 0 until 25) {
            val task = taskList.data[i]
            if (i < 15) {
                assertEquals(Priority.Medium, task.priority)
            } else {
                assertEquals(Priority.Normal, task.priority)
            }
        }

        taskList = Task.getTaskByOwner(
            owner1,
            Status.Processing,
            null,
            sortType = SortType.StatusAndPriority,
            limit = 25,
            cursor = nextCursor
        )
        nextCursor = taskList.nextCursor
        println(nextCursor)
        assertTrue(nextCursor.isNotEmpty())
        cs = nextCursor.split(SEP)
        assertEquals(3, cs.size)
        assertEquals(cs[0], Status.Processing.name)
        assertEquals(cs[1], Priority.Low.name)
        assertEquals(25, taskList.data.size)
        for (i in 0 until 25) {
            val task = taskList.data[i]
            if (i < 10) {
                assertEquals(Priority.Normal, task.priority)
            } else {
                assertEquals(Priority.Low, task.priority)
            }
        }

        taskList = Task.getTaskByOwner(
            owner1,
            Status.Processing,
            null,
            sortType = SortType.StatusAndPriority,
            limit = 25,
            cursor = nextCursor
        )
        nextCursor = taskList.nextCursor
        println(nextCursor)
        assertTrue(nextCursor.isEmpty())
        assertEquals(5, taskList.data.size)
        for (i in 0 until 5) {
            val task = taskList.data[i]
            assertEquals(Priority.Low, task.priority)
        }
    }

    @Test
    fun test06GetTaskByOwner5() {
        var taskList = Task.getTaskByOwner(owner1, null, null, sortType = SortType.StatusAndPriority, limit = 25)
        var nextCursor = taskList.nextCursor
        println(nextCursor)
        assertTrue(nextCursor.isNotEmpty())
        var cs = nextCursor.split(SEP)
        assertEquals(3, cs.size)
        assertEquals(cs[0], Status.Init.name)
        assertEquals(cs[1], Priority.Medium.name)
        assertEquals(25, taskList.data.size)
        for (i in 0 until 25) {
            val task = taskList.data[i]
            if (i < 20) {
                assertEquals(Priority.High, task.priority)
            } else {
                assertEquals(Priority.Medium, task.priority)
            }
        }

        taskList = Task.getTaskByOwner(
            owner1,
            null,
            null,
            sortType = SortType.StatusAndPriority,
            limit = 70,
            cursor = nextCursor
        )
        nextCursor = taskList.nextCursor
        println(nextCursor)
        assertTrue(nextCursor.isNotEmpty())
        cs = nextCursor.split(SEP)
        assertEquals(3, cs.size)
        assertEquals(cs[0], Status.Processing.name)
        assertEquals(cs[1], Priority.High.name)
        assertEquals(70, taskList.data.size)
        for (i in 0 until 70) {
            val task = taskList.data[i]
            if (i < 55) {
                assertEquals(Status.Init, task.status)
            } else {
                assertEquals(Status.Processing, task.status)
            }
        }

        taskList = Task.getTaskByOwner(
            owner1,
            null,
            null,
            sortType = SortType.StatusAndPriority,
            limit = 100,
            cursor = nextCursor
        )
        nextCursor = taskList.nextCursor
        println(nextCursor)
        assertTrue(nextCursor.isNotEmpty())
        cs = nextCursor.split(SEP)
        assertEquals(3, cs.size)
        assertEquals(cs[0], Status.Finished.name)
        assertEquals(cs[1], Priority.Medium.name)
        assertEquals(100, taskList.data.size)
        for (i in 0 until 100) {
            val task = taskList.data[i]
            if (i < 65) {
                assertEquals(Status.Processing, task.status)
            } else {
                assertEquals(Status.Finished, task.status)
            }
        }

        taskList = Task.getTaskByOwner(
            owner1,
            null,
            null,
            sortType = SortType.StatusAndPriority,
            limit = 100,
            cursor = nextCursor
        )
        nextCursor = taskList.nextCursor
        println(nextCursor)
        assertTrue(nextCursor.isNotEmpty())
        cs = nextCursor.split(SEP)
        assertEquals(3, cs.size)
        assertEquals(cs[0], Status.Cancel.name)
        assertEquals(cs[1], Priority.Normal.name)
        assertEquals(100, taskList.data.size)
        for (i in 0 until 100) {
            val task = taskList.data[i]
            if (i < 45) {
                assertEquals(Status.Finished, task.status)
            } else {
                assertEquals(Status.Cancel, task.status)
            }
        }

        taskList = Task.getTaskByOwner(
            owner1,
            null,
            null,
            sortType = SortType.StatusAndPriority,
            limit = 50,
            cursor = nextCursor
        )
        nextCursor = taskList.nextCursor
        println(nextCursor)
        assertTrue(nextCursor.isEmpty())
        assertEquals(25, taskList.data.size)
        for (i in 0 until 25) {
            val task = taskList.data[i]
            assertEquals(Status.Cancel, task.status)
        }
    }

    @Test
    fun test07UpdateTaskStatus() {
        var taskList = Task.getTaskByOwner(owner0, Status.Init, null, 1)
        assertEquals(1, taskList.data.size)
        var task = taskList.data[0]

        var updatedTask = Task.updateTask(task.id, task.status, Status.Processing, Priority.High, null, null)
        assertNotNull(updatedTask)
        assertEquals(Status.Processing, updatedTask.status)
        taskList = Task.getTaskByOwner(owner0, Status.Processing)
        assertEquals(1, taskList.data.size)
        task = Task.getTaskByID(task.id)!!
        assertEquals(Status.Processing, task.status)
        assertEquals(Priority.High, task.priority)

        updatedTask = Task.updateTask(task.id, updatedTask.status, Status.Finished, null, "updateTitleXX", null)
        assertNotNull(updatedTask)
        assertEquals(Status.Finished, updatedTask.status)
        taskList = Task.getTaskByOwner(owner0, Status.Finished)
        assertEquals(1, taskList.data.size)
        task = Task.getTaskByID(task.id)!!
        assertEquals(Status.Finished, task.status)
        assertEquals("updateTitleXX", task.title)

        val newDueDate = Date()
        updatedTask = Task.updateTask(task.id, updatedTask.status, Status.Cancel, Priority.Normal, null, newDueDate)
        assertNotNull(updatedTask)
        assertEquals(Status.Cancel, updatedTask.status)
        taskList = Task.getTaskByOwner(owner0, Status.Cancel)
        assertEquals(1, taskList.data.size)
        task = Task.getTaskByID(task.id)!!
        assertEquals(Status.Cancel, task.status)
        assertEquals(Priority.Normal, task.priority)
        assertEquals(Time.convertDateToSimpleString(newDueDate), task.dueDate)
    }

    companion object {
        private const val owner0 = "owner0"
        private const val owner1 = "owner1"
        private const val ownerNotExist = "ownerNotExist"
        private const val title0 = "title0"

        @BeforeClass
        @JvmStatic
        fun setup() {
            Mongo.setUp(MongoTest.Url, MongoTest.DB, true)
        }
    }
}