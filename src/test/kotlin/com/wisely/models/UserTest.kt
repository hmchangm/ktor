package com.wisely.models

import org.junit.BeforeClass
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.runners.MethodSorters
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertNull
import kotlin.test.assertTrue

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
class UserTest {

    @Test
    fun test00InsertUser() {
        var insertID = User.insertUser(email0, password0, hash0)
        assertTrue(insertID.isNotEmpty())

        insertID = User.insertUser(email0, password1, hash0)
        assertTrue(insertID.isEmpty())
    }

    @Test
    fun test01GetUserByEmail() {
        var user = User.getUserByEmail(email0)
        assertNotNull(user)
        assertEquals(email0, user.email)
        assertEquals(password0, user.password)
        assertEquals(hash0, user.hash)

        user = User.getUserByEmail(emailNotExist)
        assertNull(user)
    }

    companion object {
        private const val email0 = "email0"
        private const val password0 = "password0"
        private const val password1 = "password1"
        private const val hash0 = "hash0"
        private const val emailNotExist = "emailNotExist"

        @BeforeClass
        @JvmStatic
        fun setup() {
            Mongo.setUp(MongoTest.Url, MongoTest.DB, true)
        }
    }
}