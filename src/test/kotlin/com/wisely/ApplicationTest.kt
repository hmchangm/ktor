package com.wisely

import com.google.gson.Gson
import com.google.gson.JsonObject
import com.wisely.models.Priority
import com.wisely.models.Status
import com.wisely.models.Task
import com.wisely.routes.error.TSMCError
import com.wisely.routes.error.TSMCError.Companion.INVALID_INPUT
import com.wisely.routes.request.CreateTaskRequest
import com.wisely.routes.request.LoginRequest
import com.wisely.routes.request.RegisterRequest
import com.wisely.routes.request.UpdateTaskRequest
import com.wisely.routes.response.ErrorResponse
import com.wisely.routes.response.LoginResponse
import com.wisely.routes.response.TaskListResponse
import com.wisely.routes.response.TokenResponse
import com.wisely.utils.Time
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.plugins.*
import io.ktor.client.request.*
import io.ktor.http.*
import io.ktor.server.config.*
import io.ktor.server.testing.*
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

data class ClientResponse(
    val status: HttpStatusCode, val response: String = ""
)

class ApplicationTest {
    @Test
    fun testRegister() = testApplication {
        environment {
            config = ApplicationConfig("application-test.conf")
        }
        var body = Gson().toJson(RegisterRequest("wisely@gmail.com", "pass123456"))
        var response = clientRequest(client, HttpMethod.Post, "/api/v1/register", body)
        assertEquals(HttpStatusCode.OK, response.status)

        body = Gson().toJson(RegisterRequest("wisely@gmail.com", "pass123456"))
        response = clientRequest(client, HttpMethod.Post, "/api/v1/register", body)
        assertEquals(HttpStatusCode.BadRequest, response.status)
        var errorResponse = Gson().fromJson(response.response, ErrorResponse::class.java)
        assertEquals(TSMCError.REGISTER_USER_INSERT_FAILED, errorResponse.errorCode)

        body = Gson().toJson(RegisterRequest("wisely@gmail.com", ""))
        response = clientRequest(client, HttpMethod.Post, "/api/v1/register", body)
        assertEquals(HttpStatusCode.BadRequest, response.status)
        errorResponse = Gson().fromJson(response.response, ErrorResponse::class.java)
        assertEquals(INVALID_INPUT, errorResponse.errorCode)

        body = "{}"
        response = clientRequest(client, HttpMethod.Post, "/api/v1/register", body)
        assertEquals(HttpStatusCode.BadRequest, response.status)
        errorResponse = Gson().fromJson(response.response, ErrorResponse::class.java)
        assertEquals(INVALID_INPUT, errorResponse.errorCode)

        body = Gson().toJson(RegisterRequest("wisely2@gmail.com", "pass"))
        response = clientRequest(client, HttpMethod.Post, "/api/v1/register", body)
        assertEquals(HttpStatusCode.BadRequest, response.status)
        errorResponse = Gson().fromJson(response.response, ErrorResponse::class.java)
        assertEquals(INVALID_INPUT, errorResponse.errorCode)

        body = ""
        response = clientRequest(client, HttpMethod.Post, "/api/v1/register", body)
        assertEquals(HttpStatusCode.UnsupportedMediaType, response.status)
    }

    @Test
    fun testLogin() = testApplication {
        environment {
            config = ApplicationConfig("application-test.conf")
        }
        var body = Gson().toJson(RegisterRequest("wisely@gmail.com", "pass123456"))
        var response = clientRequest(client, HttpMethod.Post, "/api/v1/register", body)
        assertEquals(HttpStatusCode.OK, response.status)

        body = Gson().toJson(LoginRequest("wisely@gmail.com", "pass123456"))
        response = clientRequest(client, HttpMethod.Post, "/api/v1/login", body)
        assertEquals(HttpStatusCode.OK, response.status)
        var successJson = Gson().fromJson(response.response, JsonObject::class.java)
        val loginResponse = Gson().fromJson(successJson["data"], LoginResponse::class.java)
        assertTrue(loginResponse.token.isNotBlank())
        assertEquals("wisely@gmail.com", loginResponse.email)
        assertTrue((System.currentTimeMillis() + 5L * Time.Minute) < loginResponse.expired)

        body = Gson().toJson(LoginRequest("wisely2@gmail.com", "pass123456"))
        response = clientRequest(client, HttpMethod.Post, "/api/v1/login", body)
        assertEquals(HttpStatusCode.NotFound, response.status)
        var errorResponse = Gson().fromJson(response.response, ErrorResponse::class.java)
        assertEquals(TSMCError.LOGIN_USER_NOT_FOUND, errorResponse.errorCode)

        body = Gson().toJson(LoginRequest("", "pass123456"))
        response = clientRequest(client, HttpMethod.Post, "/api/v1/login", body)
        assertEquals(HttpStatusCode.BadRequest, response.status)
        errorResponse = Gson().fromJson(response.response, ErrorResponse::class.java)
        assertEquals(INVALID_INPUT, errorResponse.errorCode)

        body = Gson().toJson(LoginRequest("wisely@gmail.com", "pass1234567"))
        response = clientRequest(client, HttpMethod.Post, "/api/v1/login", body)
        assertEquals(HttpStatusCode.BadRequest, response.status)
        errorResponse = Gson().fromJson(response.response, ErrorResponse::class.java)
        assertEquals(TSMCError.LOGIN_INVALID_PASSWORD, errorResponse.errorCode)

        response = clientRequest(client, HttpMethod.Post, "/api/v1/token", null, loginResponse.token)
        assertEquals(HttpStatusCode.OK, response.status)
        successJson = Gson().fromJson(response.response, JsonObject::class.java)
        val tokenResponse = Gson().fromJson(successJson["data"], TokenResponse::class.java)
        assertTrue(tokenResponse.token.isNotBlank())
        assertTrue((System.currentTimeMillis() + 5L * Time.Minute) < tokenResponse.expired)
    }

    @Test
    fun testTask() = testApplication {
        environment {
            config = ApplicationConfig("application-test.conf")
        }
        var body = Gson().toJson(RegisterRequest("wisely@gmail.com", "pass123456"))
        var response = clientRequest(client, HttpMethod.Post, "/api/v1/register", body)
        assertEquals(HttpStatusCode.OK, response.status)

        body = Gson().toJson(LoginRequest("wisely@gmail.com", "pass123456"))
        response = clientRequest(client, HttpMethod.Post, "/api/v1/login", body)
        assertEquals(HttpStatusCode.OK, response.status)
        var successJson = Gson().fromJson(response.response, JsonObject::class.java)
        val loginResponse = Gson().fromJson(successJson["data"], LoginResponse::class.java)
        assertTrue(loginResponse.token.isNotBlank())
        assertEquals("wisely@gmail.com", loginResponse.email)
        assertTrue((System.currentTimeMillis() + 5L * Time.Minute) < loginResponse.expired)

        for (i in 0 until 100) {
            val priority = when (i % 4) {
                0 -> Priority.Low
                1 -> Priority.Normal
                2 -> Priority.Medium
                else -> Priority.High
            }
            body = Gson().toJson(CreateTaskRequest("task$i", priority.name, "2022-05-20"))
            response = clientRequest(client, HttpMethod.Post, "/api/v1/tasks", body, loginResponse.token)
            assertEquals(HttpStatusCode.OK, response.status)
            successJson = Gson().fromJson(response.response, JsonObject::class.java)
            val task = Gson().fromJson(successJson["data"], Task::class.java)
            assertEquals("task$i", task.title)
            assertEquals(loginResponse.userId, task.owner)
            assertEquals(Status.Init, task.status)
            assertEquals(priority, task.priority)
            assertEquals("2022-05-20", task.dueDate)
        }

        body = Gson().toJson(CreateTaskRequest("task0", Priority.Low.toString(), "2022-05/20"))
        response = clientRequest(client, HttpMethod.Post, "/api/v1/tasks", body, loginResponse.token)
        assertEquals(HttpStatusCode.BadRequest, response.status)
        var errorJson = Gson().fromJson(response.response, ErrorResponse::class.java)
        assertEquals(INVALID_INPUT, errorJson.errorCode)

        body = Gson().toJson(CreateTaskRequest("", Priority.Low.toString(), "2022-05-20+0800"))
        response = clientRequest(client, HttpMethod.Post, "/api/v1/tasks", body, loginResponse.token)
        assertEquals(HttpStatusCode.BadRequest, response.status)
        errorJson = Gson().fromJson(response.response, ErrorResponse::class.java)
        assertEquals(INVALID_INPUT, errorJson.errorCode)

        response = clientRequest(client, HttpMethod.Get, "/api/v1/tasks", null, loginResponse.token)
        assertEquals(HttpStatusCode.OK, response.status)
        successJson = Gson().fromJson(response.response, JsonObject::class.java)
        var taskListResponse = Gson().fromJson(successJson["data"], TaskListResponse::class.java)
        assertEquals(20, taskListResponse.list.size)
        assertTrue(taskListResponse.nextCursor.isNotBlank())

        val task0 = taskListResponse.list[0]
        val id0 = task0.id
        response = clientRequest(client, HttpMethod.Get, "/api/v1/tasks/$id0", null, loginResponse.token)
        assertEquals(HttpStatusCode.OK, response.status)
        successJson = Gson().fromJson(response.response, JsonObject::class.java)
        val task0Response = Gson().fromJson(successJson["data"], Task::class.java)
        assertEquals(id0, task0Response.id)
        assertEquals(task0.owner, task0Response.owner)
        assertEquals(task0.title, task0Response.title)
        assertEquals(task0.priority, task0Response.priority)
        assertEquals(task0.dueDate, task0Response.dueDate)

        response = clientRequest(
            client,
            HttpMethod.Get,
            "/api/v1/tasks?status=Init&priority=High&limit=30",
            null,
            loginResponse.token
        )
        assertEquals(HttpStatusCode.OK, response.status)
        successJson = Gson().fromJson(response.response, JsonObject::class.java)
        taskListResponse = Gson().fromJson(successJson["data"], TaskListResponse::class.java)
        assertEquals(25, taskListResponse.list.size)
        assertTrue(taskListResponse.nextCursor.isEmpty())
        for (t in taskListResponse.list) {
            assertEquals(Priority.High, t.priority)
        }

        response = clientRequest(
            client,
            HttpMethod.Get,
            "/api/v1/tasks?status=Init&priority=Normal&limit=20",
            null,
            loginResponse.token
        )
        assertEquals(HttpStatusCode.OK, response.status)
        successJson = Gson().fromJson(response.response, JsonObject::class.java)
        taskListResponse = Gson().fromJson(successJson["data"], TaskListResponse::class.java)
        assertEquals(20, taskListResponse.list.size)
        assertTrue(taskListResponse.nextCursor.isNotBlank())
        for (t in taskListResponse.list) {
            assertEquals(Priority.Normal, t.priority)
        }

        response = clientRequest(
            client,
            HttpMethod.Get,
            "/api/v1/tasks?status=Init&priority=Normal&limit=20&cursor=${taskListResponse.nextCursor}",
            null,
            loginResponse.token
        )
        assertEquals(HttpStatusCode.OK, response.status)
        successJson = Gson().fromJson(response.response, JsonObject::class.java)
        taskListResponse = Gson().fromJson(successJson["data"], TaskListResponse::class.java)
        assertEquals(5, taskListResponse.list.size)
        assertTrue(taskListResponse.nextCursor.isEmpty())
        for (t in taskListResponse.list) {
            assertEquals(Priority.Normal, t.priority)
        }

        response = clientRequest(client, HttpMethod.Get, "/api/v1/tasks?limit=100", null, loginResponse.token)
        assertEquals(HttpStatusCode.OK, response.status)
        successJson = Gson().fromJson(response.response, JsonObject::class.java)
        taskListResponse = Gson().fromJson(successJson["data"], TaskListResponse::class.java)
        assertEquals(100, taskListResponse.list.size)
        assertTrue(taskListResponse.nextCursor.isEmpty())

        for (i in 0 until 100) {
            val index = i % 3
            if (index == 2) {
                continue
            }
            val status = if (index == 0) Status.Processing.name else Status.Cancel.name
            val oldTask = taskListResponse.list[i]
            val taskId = oldTask.id
            body = Gson().toJson(
                UpdateTaskRequest(
                    status = status,
                    title = "update-Title$i",
                    dueDate = "2022-05-15+0000"
                )
            )
            response = clientRequest(client, HttpMethod.Patch, "/api/v1/tasks/$taskId", body, loginResponse.token)
            assertEquals(HttpStatusCode.OK, response.status)
            successJson = Gson().fromJson(response.response, JsonObject::class.java)
            val task = Gson().fromJson(successJson["data"], Task::class.java)
            assertEquals("update-Title$i", task.title)
            assertEquals(status, task.status.name)
            assertEquals(oldTask.priority, task.priority)
            assertEquals("2022-05-15", task.dueDate)
        }
    }

    private suspend fun clientRequest(
        client: HttpClient, method: HttpMethod, url: String, body: String? = null,
        token: String? = null
    ): ClientResponse {
        val clientResponse: ClientResponse = try {
            when (method) {
                HttpMethod.Get -> {
                    val response = client.get(url) {
                        header("Authorization", "Bearer $token")
                    }
                    ClientResponse(response.status, response.body<String>().toString())
                }
                HttpMethod.Post -> {
                    val response = body?.let {
                        client.post(url) {
                            contentType(ContentType.Application.Json)
                            setBody(body)
                            header("Authorization", "Bearer $token")
                        }
                    } ?: let {
                        client.post(url) {
                            header("Authorization", "Bearer $token")
                        }
                    }
                    ClientResponse(response.status, response.body<String>().toString())
                }
                HttpMethod.Patch -> {
                    val response = body?.let {
                        client.patch(url) {
                            contentType(ContentType.Application.Json)
                            setBody(body)
                            header("Authorization", "Bearer $token")
                        }
                    } ?: let {
                        client.patch(url) {
                            header("Authorization", "Bearer $token")
                        }
                    }
                    ClientResponse(response.status, response.body<String>().toString())
                }
                else -> {
                    ClientResponse(HttpStatusCode.BadRequest)
                }
            }
        } catch (e: ClientRequestException) {
            ClientResponse(e.response.status, e.response.body<String>().toString())
        }
        return clientResponse
    }
}